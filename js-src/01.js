import {readInputLines, isDigit, range} from './lib.js';


console.log('PART 1');
/** @param {string[]} input */
function solve(input) {
    const numbers = [];

    for (const line of input) {
        let firstDigit = null;
        let lastDigit = null;

        for (const ch of line.replaceAll(' ', '').split('')) {
            if (!isDigit(ch)) {
                continue;
            }

            if (firstDigit === null) {
                firstDigit = ch;
            }

            lastDigit = ch;
        }

        if (firstDigit !== null && lastDigit !== null) {
            numbers.push(+`${firstDigit}${lastDigit}`);
        }
    }

    return numbers.sum();
}

console.log('test', solve(`
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
`.split('\n')));

console.log('real', solve(readInputLines('01')));
console.log('=======================');
console.log('=======================');
console.log('PART 2');

const digits= {
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight' : 8,
    'nine': 9,
};


/** @param {string[]} input */
function solve2(input) {
    let sum = 0;

    for (const line of input) {
        const foundMatches = [];

        for (let charIdx= 0; charIdx < line.length; charIdx++) {
            for (const [key, value] of Object.entries(digits)) {
                const foundIndex = line.indexOf(key, charIdx);

                if (foundIndex !== -1) {
                    foundMatches.push({ index: foundIndex, value });
                }
            }

            for (let i = 1; i <= 9; i++) {
                const foundIndex = line.indexOf(i.toString(), charIdx);

                if (foundIndex !== -1) {
                    foundMatches.push({ index: foundIndex, value: i, });
                }
            }
        }

        if (foundMatches.length === 0) {
            continue;
        }

        foundMatches.sort((a, b) => {
            return a.index - b.index;
        });

        sum += + `${foundMatches.first().value}${foundMatches.last().value}`;
    }

    return sum;
}

console.log('test', solve2(`
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
`.split('\n')));

console.log('real', solve2(readInputLines('01')));
