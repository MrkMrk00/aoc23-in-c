import * as fs from 'node:fs';
import path from 'node:path';

Array.prototype.sum = function () {
    return this.reduce(function (prev, cur) {
        if (!isDigit(cur)) {
            throw new Error('array contains non-numbers');
        }
        return prev + cur;
    }, 0);
};

Array.prototype.first = function () {
    return this[0];
};

Array.prototype.last = function () {
    if (this.length === 0) {
        return undefined;
    }

    return this[this.length - 1];
}

export function readInputLines(name) {
    const __dirname = new URL('.', import.meta.url).pathname;
    const dir = path.dirname(__dirname);

    return fs.readFileSync(path.join(dir, 'input', name+'.txt'), 'utf-8').split('\n');
}

export function isDigit(char) {
    return typeof char === 'number' || (typeof char === 'string' && char.replaceAll(' ', '') !== '' && !isNaN(+char));
}

export function range(from, to) {
    const arr = new Array(to - from + 1);
    let idx = 0;

    for (let i = from; i <= to; i++) {
        arr[idx++] = i;
    }

    return arr;
}