import { readInputLines } from "./lib.js";

const availableCubes = {
    red: 12,
    green: 13,
    blue: 14,
};

function solve(lines) {
    const possibleGamesIds= [];

    for (const line of lines) {
        if (!line) {
            continue;
        }

        const [idPart, cubesPart] = line.split(':');
        let [_, gameId] = idPart.split(' ');
        gameId = +gameId;

        let isPossible = true;

        for (const take of cubesPart.split(';').map(t => t.trim())) {
            const cubeTakes = take
                .split(',')
                .map(take => {
                    const [count, key] = take.trim().split(' ');

                    return [key, availableCubes[key] - (+count)];
                });

            for (const [_, count] of cubeTakes) {
                if (count < 0) {
                    isPossible = false;
                    break;
                }
            }
        }

        if (isPossible) {
            possibleGamesIds.push(gameId);
        }
    }

    return possibleGamesIds.sum();
}

const testInput = `
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
`;

console.log('PART 1');
console.log('test', solve(testInput.split('\n')));

console.log('real', solve(readInputLines('02')));
console.log('=============================================');

function solve2(lines) {
    let sum = 0;

    for (const line of lines) {
        if (!line) {
            continue;
        }

        const [idPart, cubesPart] = line.split(':');
        let [_, gameId] = idPart.split(' ');
        gameId = +gameId;

        const mins = {
            red: 0,
            green: 0,
            blue: 0,
        };

        for (const take of cubesPart.split(';').map(t => t.trim())) {
            const cubeTakes = Object.fromEntries(
                take
                    .split(',')
                    .map(take => {
                        const [count, key] = take.trim().split(' ');

                        return [key, +count];
                    }),
            );

            for (const color in cubeTakes) {
                if (mins[color] < cubeTakes[color]) {
                    mins[color] = cubeTakes[color];
                }
            }
        }

        let power = 1;
        for (const color in mins) {
            power *= mins[color];
        }

        sum += power;
    }

    return sum;
}

console.log('PART 2');
console.log('test', solve2(testInput.split('\n')));
console.log('real', solve2(readInputLines('02')));
