#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "common.h"

#ifdef TEST_DATA
static const char *solution_01_test_data[] = {
    "two1nine",
    "eightwothree",
    "abcone2threexyz",
    "xtwone3four",
    "4nineeightseven2",
    "zoneight234",
    "7pqrstsixteen",
    NULL,
};
#endif


static const char *solution_02_test_data[] = {
    "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
    "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
    "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
    "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
    "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
    NULL,
};

typedef struct {
    int red, green, blue;
} BallCounts;

static const BallCounts availableCubes = {
    .red = 12,
    .green = 13,
    .blue = 14,
};

void solution_02a()
{
    #define COLON_INDEX 6
    int sum = 0;

#ifdef TEST_DATA
    int test_data_index = 0;
    const char *line;
    while ((line = solution_02_test_data[test_data_index++]) != NULL) {
#else
        INPUT("02", line)
#endif
        int line_len = strlen(line);
        if (line_len < 2) {
            continue;
        }

        int id_game = -1;
        sscanf(line, "Game %d:", &id_game);
        char previous = '\0';
        char ch;
        int line_index = strpos(line, ":") + 1;

        char number[9] = { 0 };
        int number_index = 0;

        bool is_possible = true;
        printf("Game %d ===================================\n", id_game);
        printf("%s\n", line);

        while ((ch = line[line_index++]) != '\0') {
            if (ch >= '0' && ch <= '9') {
                number[number_index++] = ch;
            }

            else if (ch >= 'a' && ch <= 'z' && previous == ' ') {
                printf("color: %c, number: %s\n", ch, number);
                int num = atoi(number);
                if (ch == 'b' && num > availableCubes.blue) {
                    printf("--- Blue impossible (%d > %d)\n", num, availableCubes.blue);
                    is_possible = false;
                    break;
                } 

                else if (ch == 'g' && num > availableCubes.green) {
                    printf("--- Green impossible (%d > %d)\n", num, availableCubes.green);
                    is_possible = false;
                    break;
                }

                else if (ch == 'r' && num > availableCubes.red) {
                    printf("--- Red impossible (%d > %d)\n", num, availableCubes.red);
                    is_possible = false;
                    break;
                }

                number_index = 0;
                memset(number, 0, 9);
            }

            previous = ch;
        }

        if (is_possible) {
            sum += id_game;
            printf("+++ Possible\n");
        }
#ifdef TEST_DATA
    }
#else
    INPUT_END
#endif

    printf("sum: %d\n", sum);
}

