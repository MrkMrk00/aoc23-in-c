#ifndef _INPUTS_H
#define _INPUTS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_FILE(fp, num)                                 \
        FILE *fp = fopen("input/" num ".txt", "r");      \
        printf("Reading from input/" num ".txt\n");      \
        if (fp == NULL) {                                   \
            fprintf(stderr, "File could not be read.");     \
            exit(EXIT_FAILURE);                             \
        }

#define END_INPUT_FILE(fp) fclose(fp);

#define BUF_LEN 2048

#define FOREACH_LINE(fp, line)              \
    char line[BUF_LEN] = { 0 };             \
    while (fgets(line, BUF_LEN, fp)) {      

#define ENDFOREACH_LINE }


#define INPUT(num, line)    \
    INPUT_FILE(fp, num)    \
    FOREACH_LINE(fp, line)

#define INPUT_END				\
    ENDFOREACH_LINE				\
    END_INPUT_FILE(fp)

#endif

static int strpos(const char* haystack, const char* needle) 
{
    char *p = strstr(haystack, needle);
    if (p == NULL) {
        return -1;
    }

    return p - haystack;
}
