#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CALL_SOLUTION(ARG, NAME)    \
    void solution_##NAME();         \
    if (strstr(ARG, #NAME) != NULL) \
        solution_##NAME();

int resolve_solution(const char *arg)
{
    CALL_SOLUTION(arg, 01a);
    CALL_SOLUTION(arg, 01b);
    CALL_SOLUTION(arg, 02a);

    return 0;
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("provide solution number\n");

        exit(1);
    }

    return resolve_solution(argv[1]);
}
