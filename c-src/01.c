#include "common.h"
#include <stdio.h>
#include <string.h>

void solution_01a()
{
    printf("Solution 1\n");

    int sum = 0;
    INPUT("01", line)
        char ch;
        char line_digits[3] = { -1, -1, 0 };
        int j = 0;

        while ((ch = line[j++]) != '\0') {
            if (ch < '0' || ch > '9') {
                continue;
            }

            if (line_digits[0] == -1) {
                line_digits[0] = ch;
            }

            line_digits[1] = ch;
        }

        sum += atoi(line_digits);
    INPUT_END

    printf("%d\n", sum);
}

typedef struct {
    char *str;
    char num;
} StrNum;

static const StrNum str_nums[9] = {
    { .str = "one",     .num = '1' },
    { .str = "two",     .num = '2' },
    { .str = "three",   .num = '3' },
    { .str = "four",    .num = '4' },
    { .str = "five",    .num = '5' },
    { .str = "six",     .num = '6' },
    { .str = "seven",   .num = '7' },
    { .str = "eight",   .num = '8' },
    { .str = "nine",    .num = '9' }
};

typedef struct {
    int index;
    char value;
} Match;

void solution_01b()
{
    printf("Solution 1b\n");
    unsigned long sum = 0;

#ifdef TEST_DATA
    const char *line;
    int line_index = 0;

    while ((line = solution_01_test_data[line_index++]) != NULL) {
#else
    INPUT("01", line)
#endif
        Match matches[63] = { 0 };
        int idx_matches = 0;

        int line_len = strlen(line);
        int last_idx = line_len - 1;

        if (line[last_idx] == '\n') {
            last_idx--;
        }

        printf("========================================\n");
        printf("line=%s; line_len=%d; last_idx=%d\n", line, line_len, last_idx);

        for (char i = '1'; i <= '9'; i++) {
            char search_string[2] = { i, '\0' };
            char *line_part = (char *) line;

            while ((line_part = strstr(line_part, search_string)) != NULL) {
                int index_of_match = line_part - line;

                printf("FOUND MATCH(1-9): %c on %d\n", i, index_of_match);
                matches[idx_matches++] = (Match) {
                    .value = i,
                    .index = index_of_match,
                };

                line_part++;
            }
        }

        for (int idx_str_num = 0; idx_str_num < 9; idx_str_num++) {
            const StrNum str_num = str_nums[idx_str_num];
            char *search_string = str_num.str;
            char *line_part = (char *) line;

            while ((line_part = strstr(line_part, search_string)) != NULL) {
                int index_of_match = line_part - line;

                printf("FOUND MATCH(one-nine): %c on %d\n", str_num.num, index_of_match);
                matches[idx_matches++] = (Match) {
                    .value = str_num.num,
                    .index = index_of_match,
                };

                line_part++;
            }
        }

        if (idx_matches == 0) {
            continue;
        exit(0);
        }

        Match first_match = { .index = -1 };
        Match last_match = { .index = -1 };
        for (int i = 0; i < idx_matches; i++) {
            Match m = matches[i];
            if (first_match.index == -1 || m.index < first_match.index) {
                first_match = m;
            }

            if (last_match.index == -1 || m.index > last_match.index) {
                last_match = m;
            }
        }

        char digits[3] = { first_match.value, last_match.value, '\0' };
        printf("digits: %s(%d)\n", digits, atoi(digits));
        sum += atoi(digits);
#ifdef TEST_DATA
    }
#else
    INPUT_END
#endif

    printf("===============================================\n");
    printf("========== Result: %lu ======================\n", sum);
    printf("===============================================\n");
}

