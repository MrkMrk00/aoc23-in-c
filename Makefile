CC=gcc
CFLAGS	:= -Wall
# LDFLAGS := 
LDLIBS	:= -lm

EXE	:= aoc
SRC_DIR := c-src
OBJ_DIR := obj
BIN_DIR	:= dest

EXE	:= $(BIN_DIR)/$(EXE)
SOURCES	:= $(wildcard $(SRC_DIR)/*.c)
OBJ	:= $(SOURCES:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

.PHONY: all clean

all: $(EXE)

$(EXE): $(OBJ) | $(BIN_DIR)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@ && chmod +x $(EXE)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(BIN_DIR) $(OBJ_DIR):
	mkdir -p $@

clean:
	$(RM) -rv $(BIN_DIR) $(OBJ_DIR)

debug:
	@echo $(SOURCES)

run: all
	@./$(EXE) 

-include $(OBJ:.o=.d)
